<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoas extends Model
{

    public $timestamps  = false;
    protected $fillable = ['id', 'nome', 'doc_identificacao', 'sexo', 'data_nascimento'];

    public function queryGraficoSexo() {

        $results = app('db')->select("select sexo, count(*) as quantidade from pessoas GROUP BY sexo");

        return $results;
    }

    public function queryGraficoIdade() {

        $results = app('db')->select("select nome, data_nascimento from pessoas");

        return $results;
    }

}