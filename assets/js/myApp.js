/**
 * Created by rodrigo on 19/10/17.
 */   

API_URL = 'http://neppo.dev/';

var app = angular.module('myApp', ['ngMask', 'ngMessages']);

app.controller('pessoasCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.crud = true;
    $scope.gerenciamentoDivs = function (type) {

        if(type == 'Crud') {
            $scope.crud = true;
            $scope.graficos = false;
        } else if(type == 'GraficosCrud') {
           $scope.crud = false;
           $scope.graficos = true;
        } else {
            $scope.crud = true;
            $scope.graficos = true;
        }

    }

    $('#datepicker').datepicker();

    /* Start Busca via API os dados das pessoas */
    $http.get(API_URL + "pessoas/")
        .then(function(response) {
            $scope.users = response.data;
        });
    /* End Busca via API os dados das pessoas */

    /* Start salvar dados pessoa no banco */
    $scope.save = function(modalstate,user_id){
        switch(modalstate){
            case 'add': var url = API_URL + "pessoas";  var method = 'POST'; break;
            case 'edit': var url = API_URL + "pessoas/"+user_id; var method = 'PUT';break;
            default: break;
        }
        $http({
            method  : method,
            url     : url,
            data    : $.param($scope.formData || ""),  // pass in data as strings
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
        }).
        then(function(response){
            location.reload();
        },function(response){
            //console.log(response);
            alert('Todos os campos são obrigatórios');
        });
    }
    /* End salvar dados pessoa no banco  */

    /* Start Delete Pessoa */
    $scope.confirmDelete = function(id){
        var isOkDelete = confirm('Você tem certeza que deseja excluir?');
        if(isOkDelete){
            $http({
                method: 'DELETE',
                url: API_URL + 'pessoas/' + id
            }).
            then(function(data){
                location.reload();
            },function (error) {
                console.log(error);
                alert('Unable to delete');
            });
        } else {
            return false;
        }
    }
    /* End Delete Pessoa */

    /* Start Show the modal */
    $scope.toggle = function(modalstate,id) {
        $scope.modalstate = modalstate;
        switch(modalstate){
            case 'add':

                $('input').val('');

                $scope.titulo = "Adicionar Pessoa";
                // $scope.pessoa_id = 0;
                $scope.nome = '';
                $scope.doc_identificacao = '';
                $scope.sexo = '';
                $scope.data_nascimento = '';

                break;
            case 'edit':
                $scope.titulo = "Detalhe Pessoa";
                $scope.pessoa_id = id;
                $http.get(API_URL + "pessoas/" + id)
                    .then(function(response) {
                        $scope.formData = response.data;
                    });
                break;
            default: break;
        }

        $('#myModal').modal('show');

    }
    /* Stop Show the modal */

    /* Start Grafico Sexo */
    $http.get(API_URL + "graficoSexo/")
        .then(function(response) {

            Highcharts.chart('graficoSexo', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Relação de Porcentagem das Pessoas em relação ao Sexo Masculino e Feminino'
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '{point.y} Pessoa(s)<b><br >{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },

                series: [{
                    colorByPoint: true,
                    data:
                        response.data

                }]
            });

        });
    /* End Grafico Sexo */

    /* Start Grafico Idades */
    $http.get(API_URL + "graficoIdade/")
        .then(function(response) {

            Highcharts.chart('graficoIdade', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Relação da Idade das pessoas por categoria de idade'
                },
                xAxis: {
                    categories: ['0 a 9', '10 a 19', '20 a 29', '30 a 39', 'Maior que 40'],
                    title: {
                        text: 'Idade'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Quantidade de Pessoas',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 50,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Pessoas',
                    data: response.data
                }]
            });

        });
    /* End Grafico Idades */

}]);