# **NEPPO - Teste Frontend Web Based** #

#Projeto a ser desenvolvido:

Desenvolver projeto web based utilizando as tecnologias html5, javascript (ecmascript 2015), css3.
Funcionalidades que deverão ser desenvolvidas:

	CRUD de pessoas
		Criar tela de pesquisa, cadastro, edição e exclusão de pessoas.
			Objeto "Pessoa" terá os seguintes atributos: nome, data nascimento, documento de identificação, sexo, endereco.
			Incluir validação de obrigatório para todos os campos: documento de identificação, nome, data nascimento e sexo.
			
	Relatorio de pessoas
		Criar tela que contenha resultados graficos das pessoas.
			Grafico da faixa de idades das pessoas, faixas: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"]
			Grafico contendo quantidade de pessoas que tenham sexo masculino e feminino
	
	Utilizar servico RESTful:
		endpoint: https://test-frontend-neppo.herokuapp.com/pessoas/
[Documentacao servico REST para referencia](https://test-frontend-neppo.herokuapp.com/apipie/): (https://test-frontend-neppo.herokuapp.com/apipie/)


#Pré requisitos, tecnologias:

	Javascript:
		- Utilizar ES6+
		- Criar projeto modularizado, aplicar conceitos ECMAScript 6 modules, AMD ou CommonJS(http://2ality.com/2014/09/es6-modules-final.html)

	HTML e CSS:
		- Utilizar HTML5
		- Utilizar CSS3(https://developer.mozilla.org/pt-BR/docs/Web/CSS/CSS3) & SASS(se preciso)
		- Desenvolver design responsivo

	UX:
		- Desenvolva pensando e aplicando conceitos de UX (http://designculture.com.br/conceitos-fundamentais-de-um-bom-ux)
	


#Hint (dicas):
	Abaixo contem algumas dicas para tornar seu projeto melhor:
		- Tente desenvolver testes unitarios
		- Tente utilizar padroes de projetos(design patterns)
		- Tente desenvolver usando o conceito single Page Application (SPA)
		- Tente usar algum framework JS, como por exemplo: Angular, Vue.js, React, AngularJS.
	  	- Tente utilizar algum framework de view, como por exemplo: bootstrap, google-material-design, metro-io
		- Tente usar Flux/Redux
		- Tente utilizar alguma ferramenta facilitadora para "unificar, minificar e obfuscar arquivos estaticos", como: webpack, gulp, grunt, etc.
		- Tente utilizar algum gerenciador de pacotes javascript(gerenciador de dependencias), como: yarn, NPM, Bower. 
		- Tente usar Typescript (se tiver conhecimento pode optar pelo desenvolvimento deste ao inves de javascript)
		- Tente ter import de apenas um arquivo JS no arquivo HTML.				
		- Tente ter import de apenas um arquivo CSS no arquivo HTML.


Crie um fork deste repo, faça o desenvolvimento necessario e nos envie a url.
Caso tenha problemas em criar um fork, siga o link [How to Fork in bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html#ForkingaRepository-HowtoForkaRepository)

Sugestão de editores de texto (IDE): [VSCODE](https://code.visualstudio.com/), [WebStorm](https://www.jetbrains.com/webstorm/), [Atom](https://atom.io/), [SublimeText](https://www.sublimetext.com/)

# Avaliação Rodrigo Rezende Soares - PHP - Neppo

## Informações
- Este projeto foi configurado num ambiente Docker, basta ter instalado o docker e docker-compose na máquina e rodar o comando ``docker-compose up``, automaticamente o ambiente estará pronto com Servidor Web Nginx, PHP 7 e Mysql.
- Para construção da API foi utilizado o Micro Framework [Lumen](https://lumen.laravel.com/) em PHP. 
- Para construção do CRUD foi utilizado o conceito de SPA, utilizando para view o framework ``Bootstrap 4.0`` e o framework Js ``Angular 1.6``.
- Foi utilizado gerenciador de pacoes NPM.
- Foi utilizado a ferramenta ``Gulp`` para unificar e minificar arquivos CSS e JS.
- Para testar API foi utilizado o programa POSTMAN.
- Desenvolvido utilizando IDE PHPStorm.

## Requisitos
- É necessário que tenha instalado na maquina: 
- [Docker e Docker Compose](https://docs.docker.com/compose/install/#prerequisites)
- [Composer](https://getcomposer.org/)
- [NPM](https://www.npmjs.com/)
- [GULP](https://gulpjs.com/)


## Ambiente e Docker 
- A configurações dos ambientes se encontra no arquivo ``docker-compose.yml``.
- Na pasta docker se encontra os arquivos necessários para configuração do NGINX e imagens do servidor PHP.
- Foi configurado o seguinte o host ``neppo.dev`` para acesso ao projeto, é necessário adicionar a seguinte linha em seus hosts(/etc/hots) ``127.0.0.1	apineppo.dev`` para interpretar o endereço, 

## Para executar em desenvolvimento
- Rodar o comando `docker-compose up` para subir ambiente.
- Rodar o comando `composer update` para baixar Framework e componentes PHP.
- Rodar o comando `php artisan migrate:refresh` dentro do container do PHP, para criar as tabelas no Mysql (Arquivo de configuração bando `.env`).
- Rodar o comando `npm install` para adicionar as bibliotecas ao projeto.
- Rodar o comando `gulp` para copiar as bibliotecas necessárias e adicionar ao projeto (automaticamenet ele já unifica e minifica o css e js).

Finalizado todos os comandos acima, basta acessa a seguinte URL `http://neppo.dev/` e será mostrado o teste. 

- Obs: Caso queira visualizar o teste hospedado na internet, segue o link do mesmo (http://www.supermoque.com.br/)

![Scheme](images/neppoCrud.png)

## API
Segue os dados para testar os serviços da API RESTful

`Obs: Todos os testes da API foram testado no programa` [POSTMAN](https://www.getpostman.com/)

- Buscar todos os registros: `GET:/pessoas` (http://neppo.dev/pessoas/)
- Efetuar busca especifica de uma pessoa pelo ID: `GET:/pessoas/1` (http://neppo.dev/pessoas/1)
- Efetuar cadastro de nova pessoa: ``POST:/pessoas`` com os parâmetros de exemplo com header ``Content-type: application/json`` :
```javascript
{
	"nome": "Rodrigo Rezende Soares",
	"doc_identificacao": "13525821",
	"sexo": "Masculino",
	"data_nascimento": "1986-04-18",
	"endereco": "Rua Jose Alves Garcia"
}
```
- Efetuar edição de pessoa existente: ``PUT:/pessoas/<id_pessoa>``com os parâmetros de exemplo com header ``Content-type: application/json``: 
```javascript
{
	"nome": "Rodrigo Rezende Soares2",
	"doc_identificacao": "135258212",
	"sexo": "Masculino",
	"data_nascimento": "1986-04-18",
	"endereco": "Rua Jose Alves Garcia2"
}
```
- Apagar o registro de uma pessoa existente: ``DELETE:/pessoas/<id_pessoa>`` exemplo: ``DELETE:/pessoas/1`` (http://neppo.dev/pessoas/5 METHOD DELETE)