var elixir = require('laravel-elixir');

elixir(function (mix) {

    mix.copy('node_modules/jquery/dist/jquery.js', 'public/assets/js/jquery.js' )
       .copy('node_modules/angular/angular.js', 'public/assets/js/angular.js' )
       .copy('node_modules/bootstrap/dist/js/bootstrap.js', 'public/assets/js/bootstrap.js' )
       .copy('node_modules/tether/dist/js/tether.js', 'public/assets/js/tether.js' )
       .copy('node_modules/highcharts/js/highcharts.js', 'public/assets/js/highcharts.js' )
       .copy('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 'public/assets/js/bootstrap-datepicker.js' )
       .copy('node_modules/ng-mask-npm/dist/ngMask.js', 'public/assets/js/ngMask.js' )
       .copy('node_modules/angular-messages/angular-messages.js', 'public/assets/js/angular-messages.js' )
       .copy('resources/assets/js/myApp.js', 'public/assets/js/myApp.js' )

       .copy('node_modules/bootstrap/dist/css/bootstrap.css', 'public/assets/css/bootstrap.css' )
       .copy('node_modules/highcharts/css/highcharts.css', 'public/assets/css/highcharts.css' )
       .copy('node_modules/tether/dist/css/tether.css', 'public/assets/css/tether.css' )
       .copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css', 'public/assets/css/bootstrap-datepicker.standalone.css' );


    mix.styles([
        'bootstrap.css',
        'highcharts.css',
        'tether.css',
        'bootstrap-datepicker.standalone.css'
    ], 'public/assets/all.css', 'public/assets/css');


    mix.scripts([
        'jquery.js',
        'angular.js',
        'tether.js',
        'bootstrap.js',
        'highcharts.js',
        'bootstrap-datepicker.js',
        'ngMask.js',
        'angular-messages.js',
        'myApp.js'
    ], 'public/assets/all.js', 'public/assets/js');

})