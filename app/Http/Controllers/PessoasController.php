<?php

namespace App\Http\Controllers;

use App\Models\Pessoas;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PessoasController extends Controller
{

    /**
     * Busca todas as pessoas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pessoas = Pessoas::all();
        return response()->json($pessoas);
    }

    /**
     * Pega uma pessoa especifica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pessoa = Pessoas::where('id', $id)->first();

        if(!empty($pessoa['id'])){
            return response()->json($pessoa);
        }
        else{
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Salva um cliente .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'doc_identificacao' => 'required',
            'sexo' => 'required',
            'data_nascimento' => 'required'
        ]);

        $pessoa = new Pessoas();
        $pessoa->nome = $request->nome;
        $pessoa->doc_identificacao = $request->doc_identificacao;
        $pessoa->sexo = $request->sexo;
        $date = new \DateTime($request->data_nascimento);
        $dd = $date->format('Y-m-d');
        $pessoa->data_nascimento = $dd;
        $pessoa->endereco = $request->endereco;
        $pessoa->save();
        return response()->json(['status' => 'success']);

    }

    /**
     * Atualiza uma pessoa.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nome' => 'required',
            'doc_identificacao' => 'required',
            'sexo' => 'required',
            'data_nascimento' => 'required'
        ]);

        $pessoa = Pessoas::find($id);
        $pessoa->nome = $request->nome;
        $pessoa->doc_identificacao = $request->doc_identificacao;
        $pessoa->sexo = $request->sexo;
        $date = new \DateTime($request->data_nascimento);
        $dd = $date->format('Y-m-d');
        $pessoa->data_nascimento = $dd;
        $pessoa->endereco = $request->endereco;
        $pessoa->save();
        return response()->json(['status' => 'success']);
    }

    /**
     * Remove uma pessoa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Pessoas::destroy($id)){
            return response()->json(['status' => 'success']);
        }
    }

    /**
     * Busca a quantidade de pessoa para mostra no grafico.
     *
     */
    public function graficoSexo() {

        $pessoa = new Pessoas();
        $results = $pessoa->queryGraficoSexo();

        $array = array();
        foreach ($results as $row) {
            $array[] = [$row->sexo, $row->quantidade];
        }

        return $array;

    }

    /**
     * Busca a quantidade de pessoa separao por categorais de idade.
     *
     */
    public function graficoIdade() {

        $pessoa = new Pessoas();
        $results = $pessoa->queryGraficoIdade();

        $idade_0_9 = 0;
        $idade_10_19 = 0;
        $idade_20_29 = 0;
        $idade_30_39 = 0;
        $idade_maior_40 = 0;

        $array = array();
        foreach ($results as $row) {
            $idade = $this->calculoIdade($row->data_nascimento);

            if($idade < 9 ) {
                $idade_0_9++;
            } else if ($idade >= 10 && $idade <= 19) {
                $idade_10_19++;
            } else if ($idade >= 20 && $idade <= 29) {
                $idade_20_29++;
            } else if ($idade >= 30 && $idade <= 39) {
                $idade_30_39++;
            } else if ($idade >= 40) {
                $idade_maior_40++;
            }

        }

        $array = [ $idade_0_9, $idade_10_19, $idade_20_29, $idade_30_39, $idade_maior_40];

        return $array;

    }

    /**
     * Faz o calculo da idade.
     *
     */
    public function calculoIdade($data) {

        // Separa em dia, mês e ano
        list($ano, $mes, $dia) = explode('-', $data);

        // Descobre que dia é hoje e retorna a unix timestamp
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        // Descobre a unix timestamp da data de nascimento do fulano
        $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

        // Depois apenas fazemos o cálculo já citado :)
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

        return $idade;
    }
}