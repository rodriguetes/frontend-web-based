<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Crud de Cadastro de Pessoas - AngularJS Single-page Application com Lumen CRUD Services</title>

    <!-- Load Bootstrap CSS -->
    <link href="{{ URL::ASSET('assets/all.css') }}" rel="stylesheet">
    {{--<link href="{{ URL::ASSET('assets/css/bootstrap.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ URL::ASSET('assets/css/tether.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ URL::ASSET('assets/css/highcharts.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ URL::ASSET('assets/css/bootstrap-datepicker.standalone.css') }}" rel="stylesheet">--}}
</head>
<body ng-app="myApp" ng-controller="pessoasCtrl">

<div class="card">
    <div class="card-header row">
        <div class="col-sm-8">
            <h5 style="padding-top: 10px">Crud Cadastro de Pessoas com Relatório</h5>
        </div>

        <div class="col-sm-4">
            <ul class="nav nav-pills justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" ng-class="{ active: isActive('/#crud')}" href="#crud" ng-click="gerenciamentoDivs('Crud')">Crud</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" ng-class="{ active: isActive('/#graficos')}" href="#graficos" ng-click="gerenciamentoDivs('GraficosCrud')">Gráficos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#crud_graficos" ng-click="gerenciamentoDivs('')">Mostrar Crud e Graficos</a>
                </li>
            </ul>
        </div>

    </div>
</div>

<div>

    <div ng-show="crud">
        <!-- Start Tabela de pessoas -->
        <table class="table table-striped">
            <thead class="thead-inverse">
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Doc. Identificação</th>
                <th>Sexo</th>
                <th>Data Nascimento</th>
                <th>Endereço</th>
                <th>
                    <button id="btn-add" class="btn btn-success btn-xs" ng-click="toggle('add',0)">Adicionar Pessoa</button>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="user in users">
                <th scope="row">@{{ user.id }}</th>
                <td>@{{ user.nome }}</td>
                <td>@{{ user.doc_identificacao }}</td>
                <td>@{{ user.sexo }}</td>
                <td>@{{ user.data_nascimento | date: 'dd/MM/yyyy' }}</td>
                <td>@{{ user.endereco }}</td>
                <td>
                    <button class="btn btn-info btn-xs btn-detail" ng-click="toggle('edit',user.id)">Editar</button>
                    <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(user.id)">Excluir</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!-- End Tabela de pessoas -->

        <!-- Start Modal para cadastro e edição de Pessoa -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">@{{titulo}}</h4>
                    </div>
                    <div class="modal-body">
                        <form name="frm" class="form-horizontal" role="form" novalidate ng-model="form">

                            <div class="form-group float-right">
                                * Campos Obrigatórios
                            </div>


                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Nome*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputnome" placeholder="Nome" value="@{{nome}}" ng-model="formData.nome" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Doc. Identificação*</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputdoc" placeholder="Documento Identificação" value="@{{doc_identificacao}}" ng-model="formData.doc_identificacao">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Endereço</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="inputendereco" placeholder="Documento Identificação" value="@{{endereco}}" ng-model="formData.endereco">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Sexo*</label>
                                <div class="col-sm-9">
                                    <input type="radio" ng-model="formData.sexo" value="Feminino">Feminino
                                    <input type="radio" ng-model="formData.sexo" value="Masculino">Masculino
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Data Nascimento*</label>
                                <div class="col-sm-9">
                                    <input type="text"  class="form-control" id="datepicker" placeholder="Data Nascimento" value="@{{data_nascimento}}" ng-model="formData.data_nascimento">
                                </div>
                            </div>

                            <button type="button" class="btn btn-warning" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-info" id="btn-save" ng-click="save(modalstate,pessoa_id)" >Salvar</button>
                        </form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal para cadastro e edição de Pessoa -->
    </div>

    <div ng-show="graficos" class="card row">
        <div class="card" id="graficoSexo">Placeholder for chart</div>
        <br /><br />
        <div class="card" id="graficoIdade">Placeholder for chart</div>
    </div>

</div>

<script src="{{ URL::ASSET('assets/all.js') }}"></script>

{{--<script src="{{ URL::ASSET('assets/js/jquery.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/angular.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/tether.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/bootstrap.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/highcharts.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/bootstrap-datepicker.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/ngMask.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/angular-messages.js') }}"></script>--}}
{{--<script src="{{ URL::ASSET('assets/js/myApp.js') }}"></script>--}}

</body>
</html>