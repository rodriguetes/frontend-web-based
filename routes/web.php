<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('index');
    //return $router->app->version();
});

$router->get('/graficoSexo', 'PessoasController@graficoSexo');
$router->get('/graficoIdade', 'PessoasController@graficoIdade');

$router->group(['prefix' => 'pessoas/'], function ($app) {
    $app->get('/','PessoasController@index'); //get all the routes
    $app->get('/{id}/', 'PessoasController@show'); //get single route
    $app->post('/','PessoasController@store'); //store single route
    $app->put('/{id}/','PessoasController@update'); //update single route
    $app->delete('/{id}/','PessoasController@destroy'); //delete single route
});
